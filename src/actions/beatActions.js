import {
  GET_ALL_BEATS,
  GET_BEATS_BY_TAG,
  GET_ALL_TAGS,
  BEATS_LOADING,
  GET_ERRORS,
  PLAY_BEAT,
  GET_TRACK_BY_USER
} from './types';
import axios from 'axios';

export const getBeatsByTag = tag => dispatch => {
  dispatch(setBeatsLoading());
  axios
    .get(`/api/tracks/tag/${tag}`)
    .then(res =>
      dispatch({
        type: GET_BEATS_BY_TAG,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getAllBeats = () => dispatch => {
  dispatch(setBeatsLoading());
  axios
    .get('/api/tracks')
    .then(res =>
      dispatch({
        type: GET_ALL_BEATS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getAllTags = () => dispatch => {
  axios
    .get(`/api/tags`)
    .then(res =>
      dispatch({
        type: GET_ALL_TAGS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};


// Profile loading
export const setBeatsLoading = () => {
  return {
    type: BEATS_LOADING
  };
};
