import axios from "axios";
import { GET_TRACK_BY_USER, GET_ERRORS } from "./types";

export const getTracksByUser = userId => dispatch => {
	console.log(userId);
	axios
		.get(`/api/tracks/user/${userId}`)
		.then(res =>
			dispatch({
				type: GET_TRACK_BY_USER,
				payload: res.data
			})
		)
		.catch(err =>
			dispatch({
				type: GET_ERRORS,
				payload: err.response.data
			})
		);
};
