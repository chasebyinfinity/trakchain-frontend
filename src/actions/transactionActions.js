import axios from "axios";
import { NEW_TRANSACTION, GET_BOUGHT, GET_SOLD, GET_ERRORS } from "./types";

export const newTransaction = transactionData => dispatch => {
  axios
    .post("/api/transactions", transactionData)
    .then(res =>
      dispatch({
        type: NEW_TRANSACTION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getBought = () => dispatch => {
  axios
    .get("/api/transactions/me/bought")
    .then(res =>
      dispatch({
        type: GET_BOUGHT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
export const getSold = () => dispatch => {
  axios
    .get("/api/transactions/me/sold")
    .then(res =>
      dispatch({
        type: GET_SOLD,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
