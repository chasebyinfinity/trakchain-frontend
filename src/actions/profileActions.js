import axios from "axios";
import {
  GET_CURRENT_PROFILE,
  GET_PROFILE_BY_HANDLE,
  GET_ERRORS,
  EDIT_PROFILE
} from "./types";

export const getCurrentProfile = () => dispatch => {
  axios
    .get("api/profiles/me")
    .then(res =>
      dispatch({
        type: GET_CURRENT_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const getProfileByHandle = handle => dispatch => {
  axios
    .get(`/api/profiles/username/${handle}`)
    .then(res =>
      dispatch({
        type: GET_PROFILE_BY_HANDLE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editProfileInformation = profileData => dispatch => {
  axios
    .post("api/profiles/", profileData)
    .then(res =>
      dispatch({
        type: EDIT_PROFILE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
