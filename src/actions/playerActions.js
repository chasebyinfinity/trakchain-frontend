import { PLAY_BEAT, GET_ERRORS } from './types';
import axios from 'axios';

export const playBeatById = trackId => dispatch => {
  
  axios
    .get(`/api/tracks/${trackId}`)
    .then(res =>
      dispatch({
        type: PLAY_BEAT,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};
