import axios from 'axios';
import { UPLOAD_TRACK, GET_ERRORS, SET_CURRENT_TRACK, EDIT_TRACK } from './types';


export const uploadTrack = (fileData) => dispatch => {

  axios.post('/api/tracks', fileData)
  .then(res => dispatch({
    type: UPLOAD_TRACK,
    payload: res.data
  })).catch(err => dispatch({
    type: GET_ERRORS,
    payload: err.response.data
  }))

}

export const editTrack = (trackId, newData) => dispatch =>  {
  axios.put(`/api/tracks/${trackId}`, newData)
  .then(res => dispatch({
    type: EDIT_TRACK,
    payload: res.data
  })).catch(err => dispatch({
    type: GET_ERRORS,
    payload: err.response.data
  }))
}

export const setCurrentTrack = (trackId) => dispatch => {
  axios.get(`/api/tracks/${trackId}`)
  .then(res => dispatch({
    type: SET_CURRENT_TRACK,
    payload: res.data
  })).catch(err=> dispatch({
    type: GET_ERRORS,
    payload: err.response.data
  }))
}
