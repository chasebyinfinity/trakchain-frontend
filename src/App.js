import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import { setCurrentUser, logoutUser } from "./actions/authActions";
import PrivateRoute from "./components/common/PrivateRoute";
import jwt_decode from "jwt-decode";
import setAuthToken from "./utils/setAuthToken";
import Landing from "./components/layout/Landing";
import Header from "./components/layout/Header";
import Login from "./components/auth/Login";
import Register from "./components/auth/Register";
import Beats from "./components/beats/Beats";
import BeatsByTag from "./components/beats/BeatsByTag";
import Dashboard from "./components/dashboard/Dashboard";
import Profile from "./components/profile/Profile";
import MyProfile from "./components/profile/MyProfile";
import Upload from "./components/upload/Upload";
import FooterAudio from "./components/layout/FooterAudio";
import Settings from "./components/settings/Settings";
import Transactions from "./components/transactions/Transactions";
import NotFound from "./components/common/NotFound";

import store from "./store";
import "./App.css";

// Check for token
if (localStorage.jwtToken) {
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());
    // Clear current Profile

    // Redirect to login
    window.location.href = "/login";
  }
}

class App extends Component {
  render() {
    return (
      <Router>
        <Provider store={store}>
          <div className="App">
            <Header />
            <div className="main-container">
              <Switch>
                <Route exact path="/" component={Landing} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/beats" component={Beats} />
                <Route exact path="/tags/:tag" component={BeatsByTag} />

                <PrivateRoute exact path="/dashboard" component={Dashboard} />

                <Route exact path="/p/:handle" component={Profile} />

                <PrivateRoute exact path="/me" component={MyProfile} />

                <PrivateRoute exact path="/upload" component={Upload} />

                <PrivateRoute exact path="/settings" component={Settings} />
                <PrivateRoute
                  exact
                  path="/transactions"
                  component={Transactions}
                />
                <Route component={NotFound} />
              </Switch>
            </div>

            <FooterAudio />
          </div>
        </Provider>
      </Router>
    );
  }
}

export default App;
