import React, { Component } from "react";
import InputGroup from "../common/InputGroup";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { loginUser } from "../../actions/authActions";

class Login extends Component {
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      console.log("true");
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/");
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      errors: {}
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    let userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData);

    // login action
    this.setState({
      email: "",
      password: ""
    });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { onSubmit, onChange } = this;
    const { error } = this.props;
    const loginError = error.errors.login;

    return (
      <div className="container">
        <div className="register-form">
          <h1>login</h1>

          <form onSubmit={onSubmit}>
            <InputGroup
              type="text"
              name="email"
              placeholder="email"
              value={this.state.email}
              /* errors={errors.email} */
              onChange={onChange}
            />
            <InputGroup
              type="password"
              name="password"
              placeholder="password"
              value={this.state.password}
              /* errors={errors.password} */
              onChange={onChange}
            />
            <p className="is-invalid">{loginError ? loginError : null}</p>
            <input
              type="submit"
              class="btn btn-block btn-dark"
              value="sign up"
            />
          </form>
        </div>
      </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  error: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  error: state.error,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
