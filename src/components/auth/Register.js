import React, { Component } from "react";
import { connect } from "react-redux";
import { registerUser } from "../../actions/authActions";
import PropTypes from "prop-types";
import InputGroup from "../common/InputGroup";

class Register extends Component {
  componentWillReceiveProps(nextProps) {
    if (nextProps.error.errors) {
      this.setState({ errors: nextProps.error.errors });
    }
  }
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      password2: "",
      role: "PRODUCER",
      errors: {}
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const newUser = {
      username: this.state.username,
      firstName: this.state.firstname,
      lastName: this.state.lastname,
      email: this.state.email,
      password: this.state.password,
      role: this.state.role
    };

    if (this.state.errros) {
      console.log(this.state.errors);
    } else {
      // this.props.registerUser(newUser);
      this.props.registerUser(newUser);
      this.setState({
        username: "",
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        password2: "",
        role: ""
      });
      this.props.history.push("/login");
    }
  }

  // registration action

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  render() {
    const { onSubmit, onChange } = this;
    const { errors } = this.state;

    return (
      <div class="container">
        <div class="register-form">
          <h1>register</h1>
          <p>
            upon public release, this will not exist and trakchain will become
            invite-only.
          </p>
          <form onSubmit={onSubmit}>
            <InputGroup
              type="text"
              name="username"
              placeholder="username"
              value={this.state.username}
              errors={errors.username}
              onChange={onChange}
            />
            <InputGroup
              type="text"
              name="firstname"
              placeholder="first name"
              value={this.state.firstname}
              errors={errors.firstname}
              onChange={onChange}
            />
            <InputGroup
              type="text"
              name="lastname"
              placeholder="last name"
              value={this.state.lastname}
              errors={errors.lastname}
              onChange={onChange}
            />
            <InputGroup
              type="text"
              name="email"
              placeholder="email"
              value={this.state.email}
              errors={errors.email}
              onChange={onChange}
            />
            <InputGroup
              type="password"
              name="password"
              placeholder="password"
              value={this.state.password}
              errors={errors.password}
              onChange={onChange}
            />
            <InputGroup
              type="password"
              name="password2"
              placeholder="confirm password"
              value={this.state.password2}
              errors={errors.password2}
              onChange={onChange}
            />
            <div className="form-group">
              <select
                name="role"
                value={this.state.role}
                onChange={onChange}
                className="form-control"
              >
                <option value="PRODUCER">Producer</option>
                <option value="CUSTOMER">Customer</option>
              </select>
            </div>
            <input
              type="submit"
              class="btn btn-block btn-dark"
              value="sign up"
            />
          </form>
        </div>
      </div>
    );
  }
}

Register.propTypes = {
  error: PropTypes.object.isRequired,
  auth: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  error: state.error,
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { registerUser }
)(Register);
