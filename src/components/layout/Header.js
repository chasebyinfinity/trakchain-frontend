import React, { Component } from "react";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

class Layout extends Component {
  constructor(props) {
    super(props);

    this.onLogout = this.onLogout.bind(this);
  }

  onLogout(e) {
    this.props.logoutUser();
  }

  render() {
    const { isAuthenticated } = this.props.auth;
    const guestLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/login">
            login
          </Link>
        </li>

        <li className="nav-item">
          <Link className="nav-link" to="/register">
            sign up
          </Link>
        </li>
      </ul>
    );
    const authLinks = (
      <ul className="navbar-nav ml-auto">
        <li className="nav-item">
          <Link className="nav-link" to="/upload">
            upload
          </Link>
        </li>

        {/*
        <li className="nav-item">
          <Link className="nav-link" to="/transactions">
            transactions
          </Link>
        </li>
        */}

        <li className="nav-item">
          <Link className="nav-link" to="/me">
            my profile
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/transactions">
            transactions
          </Link>
        </li>
        <li className="nav-item">
          <Link className="nav-link" to="/settings">
            settings
          </Link>
        </li>

        <li className="nav-item">
          <Link className="nav-link" to="/" onClick={this.onLogout}>
            logout
          </Link>
        </li>
      </ul>
    );
    return (
      <div className="container">
        <nav className="navbar navbar-expand-lg navbar-light bg-white">
          <Link className="navbar-brand" to="/">
            trakz
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <ul className="navbar-nav mr-auto">
              <li className="nav-item">
                <Link className="nav-link" to="/beats">
                  beats
                </Link>
              </li>
            </ul>

            {isAuthenticated ? authLinks : guestLinks}
          </div>
        </nav>
      </div>
    );
  }
}

Layout.propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Layout);
