import React, { Component } from "react";
import ReactAudioPlayer from "react-audio-player";
import { connect } from "react-redux";

// Props
import PropTypes from "prop-types";

// config
import config from "../../config";

class FooterAudioPlayer extends Component {
  // Instantiate Props
  constructor(props) {
    super(props);
    this.state = {
      baseUrl: config.ipfsGateway.url,
      currentSound: {},
      isPlaying: this.props.isPlaying
    };
  }

  //Upon prop update, update the state
  componentWillReceiveProps(nextProps) {
    this.setState({
      currentSound: nextProps.player.currentBeat,
      isPlaying: nextProps.player.isPlaying
    });
  }

  render() {
    console.log(this.state);
    //Construct audio source URL and render audio element. Show audio details if playing

    const { player } = this.props;
    let url = this.state.baseUrl + player.currentBeat.ipfsHash;

    if (!player.isPlaying) {
      url = "";
    }
    const footerStyle = {
      padding: "-50px 0px 0px 0px"
    };

    return (
      <div style={footerStyle} className="footer">
        <ReactAudioPlayer
          className="audio"
          src={url}
          controls
          controlsList="nodownload"
          autoPlay={player.isPlaying}
        />

        {this.state.isPlaying ? (
          <p class="currSoundInfo">
            {" "}
            <strong> {this.state.currentSound.name}</strong> -{" "}
            {this.state.currentSound.owner.name}
          </p>
        ) : (
          <p />
        )}
      </div>
    );
  }
}

FooterAudioPlayer.propTypes = {
  beat: PropTypes.object.isRequired,
  player: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  beat: state.beat,
  player: state.player,
  error: state.error
});

export default connect(mapStateToProps)(FooterAudioPlayer);
