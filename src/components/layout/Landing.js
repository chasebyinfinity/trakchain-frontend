import React, { Component } from "react";
import placeholder1 from "../../img/placeholder-mvp1.jpg";
import placeholder2 from "../../img/placeholder-mvp2.jpg";
import placeholder3 from "../../img/placeholder-mvp3.jpg";

class Landing extends Component {
  render() {
    return (
      <div class="container">
        <div
          id="carouselExampleControls"
          class="carousel slide"
          data-ride="carousel"
        >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                class="d-block w-100 mvp-img"
                src={placeholder1}
                alt="First slide"
              />
            </div>
            <div class="carousel-item">
              <img
                class="d-block w-100 mvp-img"
                src={placeholder2}
                alt="Second slide"
              />
            </div>
            <div class="carousel-item">
              <img
                class="d-block w-100 mvp-img"
                src={placeholder3}
                alt="Third slide"
              />
            </div>
          </div>
          <a
            class="carousel-control-prev"
            href="#carouselExampleControls"
            role="button"
            data-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true" />
            <span class="sr-only">Previous</span>
          </a>
          <a
            class="carousel-control-next"
            href="#carouselExampleControls"
            role="button"
            data-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true" />
            <span class="sr-only">Next</span>
          </a>
        </div>
        <div class="jumbotron jumbotron-black">
          <h1 class="display-4">beats, sounds and samples</h1>
          <p class="lead">
            trakz is an invite-only, decentralized marketplace for independent
            producers to showcase and sell their work.
          </p>
          <p class="lead">
            no one owns your work except <b>you</b>. no track limit, no fees, no
            commission, no <b>bullshit.</b>
          </p>
        </div>
      </div>
    );
  }
}

export default Landing;
