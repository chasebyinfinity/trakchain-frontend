import React, { Component } from 'react';
import spinner from '../../img/spinner.gif';

const Spinner = () => {
  const spinnerStyle = {
    width: "10%"
  }
  return(
    <img style={spinnerStyle} src={spinner} />);

};

export default Spinner;
