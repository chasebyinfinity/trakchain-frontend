import React, { Component } from "react";
import { Link } from "react-router-dom";
import uzishrug from "../../img/uzi-shrug.gif";

class NotFound extends Component {
  render() {
    return (
      <div className="container">
        <div className="register-form">
          <h1>oops</h1>
          <p>
            What you were looking for could not be found. Go back{" "}
            <Link to="/">home</Link>.
          </p>

          <img src={uzishrug} />
        </div>
      </div>
    );
  }
}

export default NotFound;
