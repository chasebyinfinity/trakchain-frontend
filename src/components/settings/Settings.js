import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getCurrentProfile,
  editProfileInformation
} from "../../actions/profileActions";
import isEmpty from "../../utils/is-empty";
import PropTypes from "prop-types";

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      bio: "",
      buyingterms: "",
      youtube: "",
      instagram: "",
      twitter: "",
      facebook: "",
      coverPhotoImage: "",
      coverPhotoIPFSHash: "",
      profilePhotoImage: "",
      profilePhotoIPFSHash: "",

      errors: {}
    };
    this.onSubmit = this.onSubmit.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onSubmit(e) {
    e.preventDefault();
    const {
      coverPhotoImage,
      profilePhotoImage,
      bio,
      youtube,
      instagram,
      facebook,
      twitter
    } = this.state;
    let formData = new FormData();
    formData.append("bio", bio);
    formData.append("youtube", youtube);
    formData.append("facebook", facebook);
    formData.append("instagram", instagram);
    formData.append("twitter", twitter);
    formData.append("coverPhoto", coverPhotoImage);
    formData.append("profileImage", profilePhotoImage);
    this.props.editProfileInformation(formData);
    this.props.history.push("/me");
  }
  onChange(e) {
    e.preventDefault();
    switch (e.target.name) {
      case "coverPhotoImage":
        this.setState({ coverPhotoImage: e.target.files[0] });
        break;

      case "profilePhotoImage":
        this.setState({ profilePhotoImage: e.target.files[0] });
        break;

      default:
        this.setState({ [e.target.name]: e.target.value });
    }
  }
  componentDidMount() {
    this.props.getCurrentProfile();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.profile) {
      const profile = nextProps.profile.profile;

      // empty strings if fields don't exist
      profile.coverPhotoIPFSHash = !isEmpty(profile.coverPhotoIPFSHash)
        ? profile.coverPhotoIPFSHash
        : "";
      profile.profileImageHash = !isEmpty(profile.profileImageHash)
        ? profile.profileImageHash
        : "";
      profile.user.username = !isEmpty(profile.user.username)
        ? profile.user.username
        : "";
      profile.bio = !isEmpty(profile.bio) ? profile.bio : "";
      profile.buyingterms = !isEmpty(profile.buyingterms)
        ? profile.buyingterms
        : "";
      profile.social = !isEmpty(profile.social) ? profile.social : {};
      profile.twitter = !isEmpty(profile.social.twitter)
        ? profile.social.twitter
        : "";
      profile.facebook = !isEmpty(profile.social.facebook)
        ? profile.social.facebook
        : "";
      profile.instagram = !isEmpty(profile.social.instagram)
        ? profile.social.instagram
        : "";
      profile.youtube = !isEmpty(profile.social.youtube)
        ? profile.social.youtube
        : "";
      this.setState({
        username: profile.user.username,
        coverPhotoIPFSHash: profile.coverPhotoIPFSHash,
        profilePhotoIPFSHash: profile.profileImageHash,
        youtube: profile.youtube,
        instagram: profile.instagram,
        facebook: profile.facebook,
        twitter: profile.twitter,
        bio: profile.bio,
        buyingterms: profile.buyingterms
      });
    }
  }
  render() {
    return (
      <div className="container">
        <div className="register-form">
          <h1>settings</h1>
          <form onSubmit={this.onSubmit}>
            <div className="input-group mb-3">
              <p>
                upload profile picture{" "}
                <input
                  type="file"
                  name="profilePhotoImage"
                  onChange={this.onChange}
                />
              </p>
            </div>

            <div className="input-group mb-3">
              <p>
                upload cover photo{" "}
                <input
                  type="file"
                  name="coverPhotoImage"
                  onChange={this.onChange}
                />
              </p>
            </div>

            <div className="input-group mb-3">
              <input type="submit" className="btn btn-dark" value="save" />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Settings.propTypes = {
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  getCurrentProfile: PropTypes.func.isRequired,
  editProfileInformation: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error,
  profile: state.profile
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, editProfileInformation }
)(Settings);
