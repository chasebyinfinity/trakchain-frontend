import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { uploadTrack } from "../../actions/trackAction";
import classnames from "classnames";

class Upload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      tags: "",
      trackImage: "",
      trackFile: "",
      price: "",
      errors: {}
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.error) {
      this.setState({ errors: nextProps.error.errors });
    }
  }
  onChange(e) {
    e.preventDefault();
    switch (e.target.name) {
      case "trackImage":
        this.setState({ trackImage: e.target.files[0] });
        break;

      case "trackFile":
        this.setState({ trackFile: e.target.files[0] });
        break;

      default:
        this.setState({ [e.target.name]: e.target.value });
    }
  }

  onSubmit(e) {
    e.preventDefault();
    const { trackImage, trackFile, name, tags, price } = this.state;
    let formData = new FormData();

    let tagsArray = [tags.split(",")];

    formData.append("fileImage", trackImage);
    formData.append("fileAudio", trackFile);
    formData.append("name", name);
    formData.append("tags", tagsArray);
    formData.append("price", price);

    this.props.uploadTrack(formData);

    if (this.state.errors.length === 0) {
      this.setState({
        name: "",
        tags: "",
        trackImage: "",
        trackFile: ""
      });

      this.props.history.push("/beats");
    }

    // console.log(tagsArray)
  }

  render() {
    const styleSelect = {
      width: "50%"
    };
    return (
      <div className="container">
        <div className="register-form">
          <h1>new trak</h1>
          <p>
            it’s dead simple, select an image, give your trak a name, tags and
            set a price
          </p>
          <form onSubmit={this.onSubmit}>
            <div className="input-group mb-3">
              <p>
                trak image{" "}
                <input
                  type="file"
                  name="trackImage"
                  id="trackImage"
                  onChange={this.onChange}
                />
              </p>
            </div>

            <div className="input-group mb-3">
              <input
                type="text"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.name
                })}
                placeholder="trak name"
                name="name"
                value={this.state.name}
                onChange={this.onChange}
              />
              {this.state.errors.name && (
                <div class="invalid-feedback">{this.state.errors.name}</div>
              )}
            </div>

            <div className="input-group mb-3">
              <input
                type="text"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.tags
                })}
                placeholder="tags"
                name="tags"
                value={this.state.tags}
                onChange={this.onChange}
              />
              {this.state.errors.tags && (
                <div class="invalid-feedback">{this.state.errors.tags} </div>
              )}
            </div>

            <div className="input-group mb-3">
              <input
                type="number"
                className={classnames("form-control", {
                  "is-invalid": this.state.errors.price
                })}
                placeholder="price (in USD)"
                name="price"
                step="any"
                value={this.state.price}
                onChange={this.onChange}
              />
              {this.state.errors.price && (
                <div class="invalid-feedback">{this.state.errors.price}</div>
              )}
            </div>

            <div className="input-group mb-3">
              <div className="input-group mb-3">
                <p>
                  select trak{" "}
                  <input
                    type="file"
                    name="trackFile"
                    onChange={this.onChange}
                  />
                </p>
              </div>
              <div className="input-group mb-3">
                <input type="submit" className="btn btn-dark" value="upload" />
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Upload.propTypes = {
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  track: PropTypes.object.isRequired,
  uploadTrack: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error,
  track: state.track
});

export default connect(
  mapStateToProps,
  { uploadTrack }
)(Upload);
