import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { getBought, getSold } from "../../actions/transactionActions";

// config
import config from "../../config";

function forceDownload(url, fileName) {
  var xhr = new XMLHttpRequest();
  xhr.open("GET", url, true);
  xhr.responseType = "blob";
  xhr.onload = function() {
    var urlCreator = window.URL || window.webkitURL;
    var imageUrl = urlCreator.createObjectURL(this.response);
    var tag = document.createElement("a");
    tag.href = imageUrl;
    tag.download = fileName;
    document.body.appendChild(tag);
    tag.click();
    document.body.removeChild(tag);
  };
  xhr.send();
}

class Transactions extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bought: [],
      sold: []
    };
  }
  componentDidMount() {
    this.props.getBought();
    this.props.getSold();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.transaction.sold) {
      this.setState({ sold: nextProps.transaction.sold });
    }
    if (nextProps.transaction.bought) {
      this.setState({ bought: nextProps.transaction.bought });
    }
  }
  render() {
    const { bought, sold } = this.state;
    let boughtContent;
    let soldContent;
    if (bought) {
      boughtContent = bought.map(tx => (
        <tr>
          <td>{tx.track.name}</td>
          <td>
            <Link to={`/p/${tx.vendor.username}`}>{tx.vendor.username}</Link>
          </td>
          <td>{tx.leaseType}</td>
          <td>${tx.track.price}</td>
          <td>
            <a
              href={config.ipfsGateway.url + tx.track.ipfsHash}
              download={tx.track.name}
            >
              download
            </a>
          </td>
        </tr>
      ));
    }

    if (sold) {
      soldContent = sold.map(tx => (
        <tr>
          <td>{tx.track.name}</td>
          <td>
            <Link to={`/p/${tx.customer.username}`}>
              {tx.customer.username}
            </Link>
          </td>
          <td>{tx.leaseType}</td>
          <td>${tx.track.price}</td>
        </tr>
      ));
    }

    return (
      <div className="container">
        <div className="register-form">
          <h1>transactions</h1>

          <h4>bought</h4>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">trak name</th>
                <th scope="col">bought from</th>
                <th scope="col">lease type</th>
                <th scope="col">price</th>
                <th scope="col">download</th>
              </tr>
            </thead>
            <tbody>{boughtContent}</tbody>
          </table>

          <h4>sold</h4>
          <table class="table">
            <thead>
              <tr>
                <th scope="col">trak name</th>
                <th scope="col">bought by</th>
                <th scope="col">lease type</th>
                <th scope="col">price</th>
              </tr>
            </thead>
            <tbody>{soldContent}</tbody>
          </table>
        </div>
      </div>
    );
  }
}

Transactions.propTypes = {
  getBought: PropTypes.func.isRequired,
  getSold: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  transaction: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  transaction: state.transaction,
  error: state.error
});

export default connect(
  mapStateToProps,
  { getBought, getSold }
)(Transactions);
