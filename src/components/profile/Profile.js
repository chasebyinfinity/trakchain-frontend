import React, { Component } from "react";
import { connect } from "react-redux";
import { getProfileByHandle } from "../../actions/profileActions";
import { getTracksByUser } from "../../actions/profileBeatActions";
import { Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import profileplaceholder from "../../img/profile-placeholder.png";
import PublicProfileBeatCard from "../beats/PublicProfileBeatCard";
import PaypalButton from "../paypal/PaypalButton";

// config
import config from "../../config";

const CLIENT = {
  sandbox:
    "ATSPjlDaImAqv73puqer3TmeXbXr-jSDAZfSLtVm4qJQQhpmjhPBFWe7gM-rURLn9SvURO4ut-5d8cgt",
  production:
    "AQTkWLn1tLavuAp-jtKrbGNCblTNMbQyBH2ntWjWSKJIXJkFNpNwyRXCcJsKSbSQ4OJt1nivBrt_UqN7"
};

const ENV = process.env.NODE_ENV === "production" ? "production" : "sandbox";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profile: {},
      userId: "",
      beats: [],
      hasAlreadyCheckedForTracks: false
    };
  }

  componentDidMount() {
    if (this.props.match.params.handle) {
      this.props.getProfileByHandle(this.props.match.params.handle);
    }

    if (this.props.profile) {
      console.log(this.props.profile);
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile.profile.user) {
      this.setState({
        profile: nextProps.profile.profile,
        userId: nextProps.profile.profile.user._id
      });
    } else {
      this.props.history.push("/not-found");
    }
    if (nextProps.profileBeat) {
      this.setState({ beats: nextProps.profileBeat.beats });
    }
  }

  render() {
    const onSuccess = payment => {
      console.log("Successful payment!", payment);
    };

    const onError = error =>
      console.log("Erroneous payment OR failed to load script!", error);

    const onCancel = data => console.log("Cancelled payment!", data);
    if (this.state.userId === this.props.auth.user._id) {
      this.props.history.push("/me");
    }
    let username;
    if (this.props.profile.profile.user) {
      username = this.props.profile.profile.user.username;
    }
    let coverPhotoIPFSHash;
    if (this.props.profile.profile.coverPhotoIPFSHash) {
      coverPhotoIPFSHash = this.props.profile.profile.coverPhotoIPFSHash;
    }
    let profilePhotoIPFSHash;
    if (this.props.profile.profile.profilePhotoIPFSHash) {
      profilePhotoIPFSHash = this.props.profile.profile.profilePhotoIPFSHash;
    }

    if (
      this.state.userId &&
      this.state.beats.length === 0 &&
      !this.state.hasAlreadyCheckedForTracks
    ) {
      this.props.getTracksByUser(this.state.userId);
      this.setState({ hasAlreadyCheckedForTracks: true });
    } else {
      console.log("fuck u");
    }

    if (this.state.beats) {
      this.state.beats.forEach(beat => {
        if (beat.owner._id !== this.state.userId) {
          this.props.getTracksByUser(this.state.userId);
        }
      });
    }
    let profileBeats;
    if (this.state.beats.length > 0) {
      profileBeats = this.state.beats.map(b => (
        <PublicProfileBeatCard client={CLIENT} env={ENV} beat={b} />
      ));
    } else {
      profileBeats = "This producer has not uploaded any tras";
    }

    const imgSrc = config.ipfsGateway.url + coverPhotoIPFSHash;
    const profileImgUrl = config.ipfsGateway.url + profilePhotoIPFSHash;

    const cardStyle = {
      width: "18rem"
    };

    const exampleBuyLink = (
      <a href="">
        <h6>mp3 lease - $15</h6>
      </a>
    );
    let socialLinks;
    if (this.props.profile.profile.social) {
      socialLinks = (
        <span>
          {this.props.profile.profile.social ? (
            <span>
              {this.props.profile.profile.social.twitter ? (
                <a
                  href={`https://twitter.com/${
                    this.props.profile.profile.social.twitter
                  }`}
                  className="profile-soc-links"
                >
                  <i className="fab fa-twitter" />
                </a>
              ) : (
                ""
              )}
              {this.props.profile.profile.social.facebook ? (
                <a
                  href={`https://facebook.com/${
                    this.props.profile.profile.social.facebook
                  }`}
                  className="profile-soc-links"
                >
                  <i className="fab fa-facebook" />
                </a>
              ) : (
                ""
              )}
              {this.props.profile.profile.social.instagram ? (
                <a
                  href={`https://instagram.com/${
                    this.props.profile.profile.social.instagram
                  }`}
                  className="profile-soc-links"
                >
                  <i className="fab fa-instagram" />
                </a>
              ) : (
                ""
              )}
            </span>
          ) : (
            ""
          )}
        </span>
      );
    }

    return (
      <div>
        <div class="col-md-12">
          <div class="profile clearfix">
            <div class="image">
              <img src={imgSrc} class="img-cover" />
              <div class="profile-information">
                <h4>
                  {" "}
                  {username} {socialLinks}
                </h4>
              </div>
            </div>
            <div class="user clearfix">
              <div class="avatar">
                <img
                  src={profileImgUrl}
                  class="img-thumbnail img-profile rounded-circle"
                />
              </div>
            </div>

            <div className="profile-meat">
              <div className="row">
                <div className="col-md-8">
                  <h1>traks</h1>
                  <div class="row">{profileBeats}</div>
                </div>
                <div class="col-md-4 right-side-profile">
                  <div class="bio">
                    <div class="card" style={cardStyle}>
                      <div class="card-body">
                        <h5 class="card-title">bio</h5>
                        <p class="card-text">
                          {this.props.profile.profile.bio
                            ? this.props.profile.profile.bio
                            : "this user has not entered a bio yet."}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="deal-info">
                    <h1>buying</h1>

                    <p>
                      {this.props.profile.profile.buyingterms
                        ? this.props.profile.profile.buyingterms
                        : "this producer hasn't written up any buying terms yet."}
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  getProfileByHandle: PropTypes.func.isRequired,
  getTracksByUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  profileBeat: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  profile: state.profile,
  error: state.error,
  profileBeat: state.profileBeat
});

export default connect(
  mapStateToProps,
  { getProfileByHandle, getTracksByUser }
)(Profile);
