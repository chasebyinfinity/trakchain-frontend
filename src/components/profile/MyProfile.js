import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getCurrentProfile,
  editProfileInformation
} from "../../actions/profileActions";
import { getTracksByUser } from "../../actions/profileBeatActions";
import { editTrack } from "../../actions/trackAction";
import PropTypes from "prop-types";
import profileplaceholder from "../../img/profile-placeholder.png";
import coverplaceholder from "../../img/cover-placeholder.jpg";
import isEmpty from "../../utils/is-empty";
import ProfileBeatCard from "../beats/ProfileBeatCard";

// config
import config from "../../config";

class MyProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      bio: "",
      buyingterms: "",
      youtube: "",
      instagram: "",
      twitter: "",
      facebook: "",
      coverPhotoIPFSHash: "",
      profilePhotoiPFSHash: "",
      editBioEnabled: false,
      editBuyingTermsEnabled: false,
      userId: "",
      currBeatId: "",
      currBeatName: "",
      currBeatImage: "",
      currBeatTags: "",
      currBeatAudio: "",
      currBeatPrice: "",

      beats: [],
      hasAlreadyCheckedForTracks: false
    };

    this.enableEditBio = this.enableEditBio.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.onSubmitTrackEdits = this.onSubmitTrackEdits.bind(this);
    this.triggerCoverPhotoDiag = this.triggerCoverPhotoDiag.bind(this);
    this.onCoverFileSelected = this.onCoverFileSelected.bind(this);
    this.submitCoverPhoto = this.submitCoverPhoto.bind(this);
    this.enableEditBuyingTerms = this.enableEditBuyingTerms.bind(this);
  }
  enableEditBuyingTerms() {
    this.setState({ editBuyingTermsEnabled: true });
  }
  enableEditBio() {
    this.setState({ editBioEnabled: true });
  }
  componentDidMount() {
    this.props.getCurrentProfile();
  }
  onChange(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmitTrackEdits(e) {
    e.preventDefault();
    const {
      currBeatName,
      currBeatImage,
      currBeatTags,
      currBeatAudio,
      currBeatId,
      currBeatPrice
    } = this.state;

    let tagsArray = currBeatTags.split(",");

    const editedTrack = {
      name: currBeatName,
      imageHash: currBeatImage,
      ipfsHash: currBeatAudio,
      tags: tagsArray,
      price: currBeatPrice
    };

    this.props.editTrack(currBeatId, editedTrack);
    // buggy, you have to press the save button twice for the changes to take, come back to this later
    setTimeout(() => {
      this.props.getTracksByUser(this.state.userId);
      document.getElementById("closeTrakModal").click();
    }, 5000);
  }

  onSubmit(e) {
    e.preventDefault();

    const profileData = {
      coverPhotoIPFSHash: this.state.coverPhotoIPFSHash,
      profilePhotoIPFSHash: this.state.profilePhotoIPFSHash,
      bio: this.state.bio,
      buyingterms: this.state.buyingterms,
      username: this.state.username,
      twitter: this.state.twitter,
      facebook: this.state.facebook,
      instagram: this.state.instagram,
      youtube: this.state.youtube
    };
    this.props.editProfileInformation(profileData);
    // console.log(profile)
    this.setState({ editBioEnabled: false });
    this.setState({ editBuyingTermsEnabled: false });
    document.getElementById("closeModal").click();
  }

  componentWillUnmount() {
    this.setState({ beats: [] });
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.profile) {
      const profile = nextProps.profile.profile;

      // empty strings if fields don't exist
      profile.coverPhotoIPFSHash = !isEmpty(profile.coverPhotoIPFSHash)
        ? profile.coverPhotoIPFSHash
        : "";
      profile.profilePhotoIPFSHash = !isEmpty(profile.profilePhotoIPFSHash)
        ? profile.profilePhotoIPFSHash
        : "";
      profile.bio = !isEmpty(profile.bio) ? profile.bio : "";
      profile.buyingterms = !isEmpty(profile.buyingterms)
        ? profile.buyingterms
        : "";
      profile.username = !isEmpty(profile.user.username)
        ? profile.user.username
        : "";
      profile.social = !isEmpty(profile.social) ? profile.social : {};
      profile.twitter = !isEmpty(profile.social.twitter)
        ? profile.social.twitter
        : "";
      profile.facebook = !isEmpty(profile.social.facebook)
        ? profile.social.facebook
        : "";
      profile.instagram = !isEmpty(profile.social.instagram)
        ? profile.social.instagram
        : "";
      profile.youtube = !isEmpty(profile.social.youtube)
        ? profile.social.youtube
        : "";
      profile.id = !isEmpty(profile.user._id) ? profile.user._id : "";
      this.setState({
        coverPhotoIPFSHash: profile.coverPhotoIPFSHash,
        profilePhotoIPFSHash: profile.profilePhotoIPFSHash,
        bio: profile.bio,
        username: profile.username,
        twitter: profile.twitter,
        facebook: profile.facebook,
        youtube: profile.youtube,
        instagram: profile.instagram,
        userId: profile.id,
        buyingterms: profile.buyingterms
      });
    }
    if (nextProps.profileBeat.beats) {
      this.setState({ beats: nextProps.profileBeat.beats });
    }
    if (nextProps.track.currTrack) {
      const currTrack = nextProps.track.currTrack;
      let tempTags = [];
      currTrack._id = !isEmpty(currTrack._id) ? currTrack._id : "";
      currTrack.name = !isEmpty(currTrack.name) ? currTrack.name : "";
      currTrack.price = !isEmpty(currTrack.price) ? currTrack.price : "";
      currTrack.tags = !isEmpty(currTrack.tags) ? currTrack.tags : "";
      currTrack.ipfsHash = !isEmpty(currTrack.ipfsHash)
        ? currTrack.ipfsHash
        : "";
      currTrack.imageHash = !isEmpty(currTrack.imageHash)
        ? currTrack.imageHash
        : "";
      Array.from(currTrack.tags).map(tag => tempTags.push(tag.name));
      this.setState({
        currBeatId: currTrack._id,
        currBeatName: currTrack.name,
        currBeatTags: tempTags.join(", "),
        currBeatImage: currTrack.imageHash,
        currBeatAudio: currTrack.ipfsHash,
        currBeatPrice: currTrack.price
      });
    }
  }

  triggerCoverPhotoDiag(e) {
    e.preventDefault();
    document.getElementById("coverPhotoUpload").click();
  }

  onCoverFileSelected(e) {
    e.preventDefault();
    this.setState({ [e.target.name]: e.target.files[0] });
    this.submitCoverPhoto();
  }

  submitCoverPhoto() {
    let formData = new FormData();
    formData.append("bio", this.state.bio);
    formData.append("username", this.state.username);
    formData.append("twitter", this.state.twitter);
    formData.append("facebook", this.state.facebook);
    formData.append("instagram", this.state.instagram);
    formData.append("youtube", this.state.youtube);
    formData.append("coverPhoto", this.state.coverPhoto);
    this.props.editProfileInformation(formData);
    setTimeout(() => {
      this.props.editProfileInformation(formData);
    }, 5000);
  }

  render() {
    {
      console.log(this.state);
    }
    // bug fixed

    if (
      this.state.userId &&
      this.state.beats.length === 0 &&
      !this.state.hasAlreadyCheckedForTracks
    ) {
      this.props.getTracksByUser(this.state.userId);
      this.setState({ hasAlreadyCheckedForTracks: true });
    }
    if (this.state.beats) {
      this.state.beats.forEach(beat => {
        if (beat.owner._id !== this.state.userId) {
          this.props.getTracksByUser(this.state.userId);
        }
      });
    }
    if (this.state.currBeatImage) {
      console.log(this.state.currBeatImage);
    }

    const { username } = this.state;
    const { coverPhotoIPFSHash, profilePhotoIPFSHash } = this.state;

    let currBeatTagsArray = [];
    let profileBeats;
    if (this.state.beats.length > 0) {
      profileBeats = this.state.beats.map(b => <ProfileBeatCard beat={b} />);
    } else {
      profileBeats = "You have not uploaded any traks yet.";
    }
    const displayInline = {
      display: "inline"
    };
    const socialLinks = (
      <span>
        {this.state.twitter ? (
          <a
            href={`https://twitter.com/${this.state.twitter}`}
            className="profile-soc-links"
          >
            <i className="fab fa-twitter" />
          </a>
        ) : (
          ""
        )}
        {this.state.facebook ? (
          <a
            href={`https://twitter.com/${this.state.facebook}`}
            className="profile-soc-links"
          >
            <i className="fab fa-facebook" />
          </a>
        ) : (
          ""
        )}
        {this.state.instagram ? (
          <a
            href={`https://twitter.com/${this.state.instagram}`}
            className="profile-soc-links"
          >
            <i className="fab fa-instagram" />
          </a>
        ) : (
          ""
        )}
        {this.state.youtube ? (
          <a
            href={`https://twitter.com/${this.state.instagram}`}
            className="profile-soc-links"
          >
            <i className="fab fa-youtube" />
          </a>
        ) : (
          ""
        )}
      </span>
    );
    const coverImgUrl = config.ipfsGateway.url + coverPhotoIPFSHash;
    const profileImgUrl = config.ipfsGateway.url + profilePhotoIPFSHash;
    const cardStyle = {
      width: "18rem"
    };
    const uploadButtonStyle = {
      display: "none"
    };

    return (
      <div>
        <div className="col-md-12">
          <div className="profile clearfix">
            <div className="image">
              <form ref="coverPhotoForm" onSubmit={this.submitCoverPhoto}>
                <input
                  style={uploadButtonStyle}
                  ref="coverPhotoUpload"
                  id="coverPhotoUpload"
                  type="file"
                  onChange={this.onCoverFileSelected}
                  name="coverPhoto"
                />
              </form>
              <img src={coverImgUrl} className="img-cover" />
              <div className="caption" />
              <div className="profile-information">
                <h4>
                  {" "}
                  {username} {socialLinks}{" "}
                  <a
                    href="#"
                    data-toggle="modal"
                    data-target="#editSocialModal"
                  >
                    <p style={displayInline} className="lead">
                      edit profile
                    </p>
                  </a>
                </h4>
              </div>
            </div>
            <div className="user clearfix">
              <div className="avatar">
                <img
                  src={profileImgUrl}
                  className="img-thumbnail img-profile rounded-circle"
                />
              </div>
            </div>

            <div className="profile-meat">
              <div className="row">
                <div className="col-md-8">
                  <h1>traks</h1>

                  <div className="row">{profileBeats}</div>
                </div>
                <div className="col-md-4 right-side-profile">
                  <div className="bio">
                    <div className="card" style={cardStyle}>
                      <div className="card-body">
                        <h5 className="card-title">bio</h5>
                        <p className="card-text">
                          {this.state.editBioEnabled ? (
                            <form onSubmit={this.onSubmit}>
                              <div className="form-group">
                                <textarea
                                  name="bio"
                                  className="form-control"
                                  placeholder="write a few sentences about yourself"
                                  onChange={this.onChange}
                                  value={this.state.bio}
                                />
                              </div>
                              <input
                                type="submit"
                                className="btn btn-block btn-dark"
                                value="save"
                              />
                            </form>
                          ) : (
                            <span>
                              {this.state.bio
                                ? this.state.bio
                                : "Your bio is empty."}
                              <p>
                                <a href="#" onClick={this.enableEditBio}>
                                  edit
                                </a>
                              </p>
                            </span>
                          )}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="deal-info">
                    <h1>buying</h1>

                    <p>
                      {this.state.editBuyingTermsEnabled ? (
                        <form onSubmit={this.onSubmit}>
                          <div className="form-group">
                            <textarea
                              name="buyingterms"
                              className="form-control"
                              placeholder="write up terms of agreement between you and potential buyers"
                              onChange={this.onChange}
                              value={this.state.buyingterms}
                            />
                          </div>
                          <input
                            type="submit"
                            className="btn btn-block btn-dark"
                            value="save"
                          />
                        </form>
                      ) : (
                        <span>
                          {this.state.buyingterms
                            ? this.state.buyingterms
                            : "you have no written up any buying terms."}
                          <p>
                            <a href="#" onClick={this.enableEditBuyingTerms}>
                              edit
                            </a>
                          </p>
                        </span>
                      )}
                    </p>
                    <p />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal fade"
          id="editTrackModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  edit trak
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <form onSubmit={this.onSubmitTrackEdits}>
                  <div class="input-group mb-3">
                    <input
                      type="button"
                      class="btn btn-dark"
                      value="upload new trak image"
                    />
                  </div>

                  <div class="input-group mb-3">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="edit trak name"
                      name="currBeatName"
                      onChange={this.onChange}
                      value={this.state.currBeatName}
                    />
                  </div>
                  <div class="input-group mb-3">
                    <input
                      type="number"
                      step="any"
                      class="form-control"
                      placeholder="edit trak price"
                      name="currBeatPrice"
                      onChange={this.onChange}
                      value={this.state.currBeatPrice}
                    />
                  </div>
                  <div class="input-group mb-3">
                    <input
                      type="text"
                      class="form-control"
                      placeholder="tags"
                      name="currBeatTags"
                      onChange={this.onChange}
                      value={this.state.currBeatTags}
                    />
                  </div>

                  <div class="input-group mb-3">
                    <input type="submit" class="btn btn-dark" value="save" />
                  </div>
                </form>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  id="closeTrakModal"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>

        <div
          className="modal fade"
          id="editSocialModal"
          tabindex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  edit profile
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>

              <div className="modal-body">
                <div className="row">
                  <div className="col-md-12">
                    <form onSubmit={this.onSubmit}>
                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon1">
                            <i className="fab fa-twitter" />
                          </span>
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="twitter"
                          name="twitter"
                          aria-label="twitter"
                          value={this.state.twitter}
                          onChange={this.onChange}
                          aria-describedby="basic-addon1"
                        />
                      </div>

                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon1">
                            <i className="fab fa-facebook" />
                          </span>
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="facebook"
                          name="facebook"
                          aria-label="facebook"
                          value={this.state.facebook}
                          onChange={this.onChange}
                          aria-describedby="basic-addon1"
                        />
                      </div>

                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon1">
                            <i className="fab fa-instagram" />
                          </span>
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="instagram"
                          name="instagram"
                          aria-label="instagram"
                          value={this.state.instagram}
                          onChange={this.onChange}
                          aria-describedby="basic-addon1"
                        />
                      </div>

                      <div className="input-group mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text" id="basic-addon1">
                            <i className="fab fa-youtube" />
                          </span>
                        </div>
                        <input
                          type="text"
                          className="form-control"
                          placeholder="youtube"
                          name="youtube"
                          value={this.state.youtube}
                          onChange={this.onChange}
                          aria-label="instagram"
                          aria-describedby="basic-addon1"
                        />
                      </div>

                      <input
                        type="submit"
                        className="btn btn-dark btn-block"
                        value="save"
                      />
                    </form>
                  </div>
                </div>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  id="closeModal"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MyProfile.propTypes = {
  getCurrentProfile: PropTypes.func.isRequired,
  editProfileInformation: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  profile: PropTypes.object.isRequired,
  profileBeat: PropTypes.object.isRequired,
  track: PropTypes.object.isRequired,
  getTracksByUser: PropTypes.func.isRequired,
  editTrack: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  error: state.error,
  profile: state.profile,
  profileBeat: state.profileBeat,
  track: state.track
});

export default connect(
  mapStateToProps,
  { getCurrentProfile, editProfileInformation, getTracksByUser, editTrack }
)(MyProfile);
