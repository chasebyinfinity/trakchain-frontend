import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setCurrentTrack } from "../../actions/trackAction";
import { playBeatById } from "../../actions/playerActions";

// config
import config from "../../config";

class BeatCard extends React.Component {
  constructor(props) {
    super(props);
    this.onClickBeatPlay = this.onClickBeatPlay.bind(this);
    this.setCurrentTrackForEdit = this.setCurrentTrackForEdit.bind(this);
  }

  onClickBeatPlay(trackId) {
    return e => {
      e.preventDefault();

      this.props.playBeatById(trackId);
    };
  }

  setCurrentTrackForEdit(trackId) {
    return e => {
      e.preventDefault();

      this.props.setCurrentTrack(trackId);
    };
  }

  render() {
    const playButtonStyle = {
      opacity: ".7"
    };
    const beatNameStyle = {
      margin: "auto"
    };
    let myStyle = { width: "200px" };
    let textStyle = { width: "1000%" };
    const { beat } = this.props;
    return (
      <div className="col-md-3">
        <div className="playable-tile">
          <div className="card" style={myStyle}>
            <img
              className="card-img-top profile-beat-image-main"
              src={config.ipfsGateway.url + beat.imageHash}
              alt="Card image cap"
            />
            <div className="tile-overlay">
              <button
                style={playButtonStyle}
                className="play-button btn btn-outline-dark"
                onClick={this.onClickBeatPlay(beat._id)}
              >
                play
              </button>
              <button
                style={playButtonStyle}
                className="play-button btn btn-outline-dark"
                onClick={this.setCurrentTrackForEdit(beat._id)}
                data-toggle="modal"
                data-target="#editTrackModal"
              >
                edit
              </button>
            </div>
          </div>
          <div className="card-body">
            <p className="card-text" style={textStyle}>
              <Link style={beatNameStyle} to="/">
                {beat.name}
              </Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

BeatCard.propTypes = {
  playBeatById: PropTypes.func.isRequired,
  setCurrentTrack: PropTypes.func.isRequired
};

export default connect(
  null,
  {
    playBeatById,
    setCurrentTrack
  }
)(BeatCard);
