import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { playBeatById } from "../../actions/playerActions";

import config from "../../config";

class BeatCard extends React.Component {
  constructor(props) {
    super(props);
    this.onClickBeatPlay = this.onClickBeatPlay.bind(this);
  }

  onClickBeatPlay(trackId) {
    return e => {
      e.preventDefault();
      console.log("TRACK ID", trackId);
      this.props.playBeatById(trackId);
    };
  }

  render() {
    const playButtonStyle = {
      opacity: ".7"
    };
    let myStyle = { width: "200px" };
    const { beat } = this.props;
    return (
      <div className="col-md-3">
        <div className="playable-tile">
          <div className="card" style={myStyle}>
            <img
              className="card-img-top beat-image-main"
              src={config.ipfsGateway.url + beat.imageHash}
              alt="Card image cap"
            />
            <div className="tile-overlay">
              <button
                style={playButtonStyle}
                className="play-button btn btn-outline-dark"
                onClick={this.onClickBeatPlay(beat._id)}
              >
                play
              </button>
            </div>
          </div>
          <div className="card-body">
            <p className="card-text">
              <Link to="/">{beat.name}</Link>
              <br />
              <Link to={`/p/${beat.owner.name}`}>by {beat.owner.name}</Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

BeatCard.propTypes = {
  playBeatById: PropTypes.func.isRequired
};

export default connect(
  null,
  {
    playBeatById
  }
)(BeatCard);
