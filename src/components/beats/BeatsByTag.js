import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { getBeatsByTag } from "../../actions/beatActions";
import PropTypes from "prop-types";
import placeholder1 from "../../img/placeholder-mvp1.jpg";
import Spinner from "../common/Spinner";
import BeatCard from "./BeatCard";

class BeatsByTag extends Component {
  componentDidMount() {
    if (this.props.match.params.tag) {
      this.props.getBeatsByTag(this.props.match.params.tag);
    }
  }

  render() {
    const { beats, loading } = this.props.beat;
    const { tag } = this.props.match.params;
    const cardStyle = {
      width: "200px"
    };
    let content;

    if (beats === null || loading) {
      content = <Spinner />; // Loading animation this shit
    } else {
      content = beats.map(b => <BeatCard beat={b} />);
    }

    return (
      <div className="container">
        <div class="hot-beats">
          <h4>showing beats from #{tag}</h4>
          <div class="row">
            <div class="row">{content}</div>
          </div>
        </div>
      </div>
    );
  }
}

BeatsByTag.propTypes = {
  beat: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired,
  getBeatsByTag: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  beat: state.beat,
  error: state.error
});
export default connect(
  mapStateToProps,
  { getBeatsByTag }
)(BeatsByTag);
