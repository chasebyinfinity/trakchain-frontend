import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {
  getBeatsByTag,
  getAllBeats,
  getAllTags
} from "../../actions/beatActions";
import { playBeatById } from "../../actions/playerActions";
import PropTypes from "prop-types";
import placeholder1 from "../../img/placeholder-mvp1.jpg";

// components
import BeatCard from "./BeatCard";

class Beats extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errors: {},
      beats: []
    };
    this.onClickBeatPlay = this.onClickBeatPlay.bind(this);
  }
  componentDidMount() {
    this.props.getAllTags();
    this.props.getAllBeats();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps) {
      this.setState({ errors: nextProps.error });
    }
    if (nextProps.beat) {
      this.setState({ beats: nextProps.beat.beats });
    }
  }

  compoentDidUpdate(prevProps) {
    if (prevProps.location.pathname !== this.props.location.pathname) {
      this.props.getAllTags();
      this.props.getAllBeats();
    }
  }

  onClickBeatPlay(trackId) {
    return e => {
      e.preventDefault();
      console.log("TRACK ID", trackId);
      this.props.playBeatById(trackId);
    };
  }

  render() {
    const { beat } = this.props;
    const { beats, loading } = beat;

    let tagContent;
    if (beat.tags) {
      let tags = beat.tags;
      tagContent = tags.map(tag => (
        <div className="col">
          <div className="card card-beatpage">
            <div className="card-tag">
              <div className="card-body">
                <Link to={`/tags/${tag.name}`}>{tag.name}</Link>
              </div>
            </div>
          </div>
        </div>
      ));
    } else {
      tagContent = "Nah";
    }
    let myStyle = { width: "200px" };

    let allBeatsContent;
    if (this.state.beats) {
      let allBeats = this.state.beats;
      let rowFactor = allBeats.length % 4;
      allBeatsContent = allBeats.map(beat => <BeatCard beat={beat} />);
    }
    return (
      <div className="container">
        <div className="tag-categories">
          <h4>popular tags</h4>

          <div className="row">{tagContent}</div>
        </div>
        <br />
        <div className="hot-beats">
          <h4>hot 🔥</h4>
          <div className="row">{allBeatsContent}</div>
        </div>
      </div>
    );
  }
}
Beats.propTypes = {
  getBeatsByTag: PropTypes.func.isRequired,
  getAllBeats: PropTypes.func.isRequired,
  playBeatById: PropTypes.func.isRequired,
  beat: PropTypes.object.isRequired,
  error: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  beat: state.beat,
  error: state.error
});

export default connect(
  mapStateToProps,
  {
    getBeatsByTag,
    getAllBeats,
    getAllTags,
    playBeatById
  }
)(Beats);
