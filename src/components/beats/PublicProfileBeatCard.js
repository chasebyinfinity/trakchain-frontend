import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { setCurrentTrack } from "../../actions/trackAction";
import { playBeatById } from "../../actions/playerActions";
import { newTransaction } from "../../actions/transactionActions";
import PaypalButton from "../paypal/PaypalButton";
import isEmpty from "../../utils/is-empty";

import { saveAs } from "file-saver";

// config
import config from "../../config";

function forceDownload(blob, filename) {
  var a = document.createElement("a");
  a.download = filename;
  a.href = blob;
  a.click();
}

// Current blob size limit is around 500MB for browsers
function downloadResource(url, filename) {
  if (!filename)
    filename = url
      .split("\\")
      .pop()
      .split("/")
      .pop();
  fetch(url, {
    headers: new Headers({
      Origin: window.location.origin
    }),
    mode: "cors"
  })
    .then(response => response.blob())
    .then(blob => {
      let blobUrl = window.URL.createObjectURL(blob);
      forceDownload(blobUrl, filename);
    })
    .catch(e => console.error(e));
}

class PublicProfileBeatCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      paymentSuccessful: false,
      customerId: !isEmpty(this.props.auth.user._id)
        ? this.props.auth.user._id
        : "",
      vendorId: this.props.beat.owner._id,
      trackId: this.props.beat._id
    };
    this.onClickBeatPlay = this.onClickBeatPlay.bind(this);
    this.setCurrentTrackForEdit = this.setCurrentTrackForEdit.bind(this);
  }

  onClickBeatPlay(trackId) {
    return e => {
      e.preventDefault();

      this.props.playBeatById(trackId);
    };
  }

  setCurrentTrackForEdit(trackId) {
    return e => {
      e.preventDefault();

      this.props.setCurrentTrack(trackId);
    };
  }

  render() {
    const { beat } = this.props;
    const beatDownloadURI = config.ipfsGateway.url + beat.ipfsHash;
    const onSuccess = payment => {
      console.log("Payment executed successfully!", payment);
      this.setState({ paymentSuccessful: true });
      //  const downloadLink = document.getElementById("downloadLink");
      //  downloadLink.click();
      downloadResource(beatDownloadURI, beat.name);
      const transactionData = {
        customerId: this.state.customerId,
        vendorId: this.state.vendorId,
        trackId: this.state.trackId
      };
      this.props.newTransaction(transactionData);
    };

    const onError = error =>
      console.log("Erroneous payment OR failed to load script!", error);

    const onCancel = data => console.log("Cancelled payment!", data);
    const playButtonStyle = {
      opacity: ".7"
    };
    const beatNameStyle = {
      margin: "auto"
    };
    let myStyle = { width: "200px" };
    let textStyle = { width: "1000%" };
    let downloadStyle = {
      display: "none"
    };
    var priceWithFee = beat.price * 1.03;
    console.log(priceWithFee.toFixed(2));

    return (
      <div className="col-md-3">
        <div className="playable-tile">
          <div className="card" style={myStyle}>
            <img
              className="card-img-top profile-beat-image-main"
              src={config.ipfsGateway.url + beat.imageHash}
              alt="Card image cap"
            />
            <div className="tile-overlay">
              <button
                style={playButtonStyle}
                className="play-button btn btn-outline-dark"
                onClick={this.onClickBeatPlay(beat._id)}
              >
                play
              </button>

              <PaypalButton
                client={this.props.client}
                env={this.props.env}
                commit={true}
                currency={"USD"}
                total={priceWithFee.toFixed(2)}
                onSuccess={onSuccess}
                onError={onError}
                onCancel={onCancel}
              />
            </div>
          </div>
          <div className="card-body">
            <p className="card-text" style={textStyle}>
              <Link style={beatNameStyle} to="/">
                {beat.name} - ${beat.price}
              </Link>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

PublicProfileBeatCard.propTypes = {
  auth: PropTypes.object.isRequired,
  transaction: PropTypes.object.isRequired,
  playBeatById: PropTypes.func.isRequired,
  setCurrentTrack: PropTypes.func.isRequired,
  newTransaction: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  transaction: state.transaction
});

export default connect(
  mapStateToProps,
  {
    playBeatById,
    setCurrentTrack,
    newTransaction
  }
)(PublicProfileBeatCard);
