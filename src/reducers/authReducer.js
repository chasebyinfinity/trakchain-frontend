import { CREATE_NEW_USER, SET_CURRENT_USER } from "../actions/types";
import isEmpty from "../utils/is-empty";

const initialState = {
  user: {},
  users: [],
  isAuthenticated: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case CREATE_NEW_USER:
      return {
        user: action.payload
      };

    case SET_CURRENT_USER:
      return {
        user: action.payload,
        isAuthenticated: !isEmpty(action.payload)
      };

    default:
      return state;
  }
}
