import { GET_CURRENT_PROFILE, GET_PROFILE_BY_HANDLE, EDIT_PROFILE } from '../actions/types';
const initialState = {
  profile: {},
  profiles: [],
  loading: false



};


export default function(state = initialState, action) {
  switch(action.type) {

    case EDIT_PROFILE:
      return {
        ...state,
        profile: action.payload
      }

    case GET_CURRENT_PROFILE:
      return {
        ...state,
        profile: action.payload
      }
    case GET_PROFILE_BY_HANDLE:
      return {
        ...state,
        profile: action.payload
      }

    default:
      return state;
  }

}
