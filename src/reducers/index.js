import { combineReducers } from "redux";
import beatReducer from "./beatReducer";
import errorReducer from "./errorReducer";
import authReducer from "./authReducer";
import profileReducer from "./profileReducer";
import trackReducer from "./trackReducer";
import playerReducer from "./playerReducer";
import profileBeatsReducer from "./profileBeatsReducer";
import transactionReducer from "./transactionReducer";

export default combineReducers({
  beat: beatReducer,
  error: errorReducer,
  auth: authReducer,
  profile: profileReducer,
  track: trackReducer,
  player: playerReducer,
  profileBeat: profileBeatsReducer,
  transaction: transactionReducer
});
