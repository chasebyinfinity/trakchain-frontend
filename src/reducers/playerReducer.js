import { PLAY_BEAT } from '../actions/types';

const initialState = {
  currentBeat: {},
  isPlaying: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case PLAY_BEAT:
      return {
        ...state,
        currentBeat: action.payload,
        isPlaying: true,
        loading: false
      };
    default:
      return state;
  }
}
