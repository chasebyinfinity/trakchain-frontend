import { NEW_TRANSACTION, GET_BOUGHT, GET_SOLD } from "../actions/types";
const initialState = {
  bought: [],
  sold: []
};

export default function(state = initialState, action) {
  switch (action.type) {
    case GET_BOUGHT:
      return {
        ...state,
        bought: action.payload
      };
    case GET_SOLD:
      return {
        ...state,
        sold: action.payload
      };
    default:
      return state;
  }
}
