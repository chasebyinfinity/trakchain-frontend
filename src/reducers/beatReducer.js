import {
  GET_ALL_BEATS,
  GET_BEATS_BY_TAG,
  GET_ALL_TAGS,
  BEATS_LOADING,
  PLAY_BEAT,
  GET_TRACK_BY_USER
} from '../actions/types';

const initialState = {
  tags: [],
  beats: [],
  currentBeat: {},
  isPlaying: false,
  loading: false
};

export default function(state = initialState, action) {
  switch (action.type) {
    case BEATS_LOADING:
      return {
        ...state,
        loading: true
      };
    case GET_BEATS_BY_TAG:
      return {
        ...state,
        beats: action.payload,
        loading: false
      };
    case GET_ALL_BEATS:
      return {
        ...state,
        beats: action.payload,
        loading: false
      };

    case GET_ALL_TAGS:
      return {
        ...state,
        tags: action.payload,
        loading: false
      };
    case PLAY_BEAT:
      return {
        ...state,
        currentBeat: action.payload,
        isPlaying: true,
        loading: false
      };

    case GET_TRACK_BY_USER:
      return {
        beats: action.payload
      }


    default:
      return state;
  }
}
