import { GET_ERRORS, UPLOAD_TRACK, SET_CURRENT_TRACK, EDIT_TRACK } from '../actions/types';
const initialState = {
  tracks: [],
  currTrack: {},
  errors: {}

};


export default function(state = initialState, action) {
  switch(action.type) {

    case EDIT_TRACK:
      return {
        ...state,
        currTrack: action.payload
      }

    case SET_CURRENT_TRACK:
      return {
        currTrack: action.payload
      }

    case UPLOAD_TRACK:
      return {
        tracks: action.payload
      }


    default:
      return state;
  }

}
