import { GET_TRACK_BY_USER } from '../actions/types';
const initialState = {
  beats: [],
  loading: false

};


export default function(state = initialState, action) {
  switch(action.type) {

    case GET_TRACK_BY_USER:
      return {
        beats: action.payload
      }
    default:
      return state;
  }

}
